function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function tob64(litem) {
    var ch = litem.children();
    var title = ch[0].innerText;
    var artist = ch[1].innerText;

    return {
            'b64' : Base64.toBase64(artist + ' - ' + title),
            'title' : title,
            'artist' : artist
           };
}

function fromb64(b64) {
    var fs = Base64.fromBase64(b64);

    return {
            'res' : fs,
            'title' : fs.slice(0, fs.indexOf(' - ')),
            'artist' : fs.slice(fs.indexOf(' - ') + 3, fs.length)
           };
}

function nextIndex(index, modulo) {
    newIndex = index % modulo;
    return newIndex >= 0 ? newIndex : newIndex + modulo;
}

function errorToast(message) {
    var mSect = $('#desc');
    mSect.text(message);
    launchToast();
}

function exportColors() {
    var colors = Array.from(document.getElementsByClassName("jscolor"), (i) => i.value); 
    var names = ["@text-active-color", "@inactive-color", "@center-color", "@sidebar-color", "@search-bar-color", "@bottom-bar-color", "@slider-future-color", "@slider-past-color"]
    var result = ''; 
    names.forEach((key, idx) => result += `"${key}": "#${colors[idx]}",\n`); 
    
    copyToClipboard(result.slice(0, -2)); 
}

function copyToClipboard(text) {
    const el = document.createElement('textarea');
    el.value = text;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

function formatSeconds(time) {
    // Hours, minutes and seconds
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = ~~time % 60;

    // Output like "03:19:27"
    var ret = "";
    ret += (hrs < 10 ? "0" : "") + hrs + ":";
    ret += "" + (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
    return ret;
}

function handleErrors(response) {
    if (!response.ok) {
        errorToast(errors[404]);
    }
    return response;
}

function handleMediaError(e) {
    if (e.target.error.message.includes("Empty src attribute")) {
        return;
    }
    document.getElementById("pp-button").firstElementChild.innerText = "play_arrow";
    player.pause();
    switch (e.target.error.code) {
        case e.target.error.MEDIA_ERR_ABORTED:
            errorToast('You aborted the media playback.'); break;
        case e.target.error.MEDIA_ERR_NETWORK:
            errorToast('A network error caused the media download to fail.'); break;
        case e.target.error.MEDIA_ERR_DECODE:
            errorToast('The media playback was aborted due to a corruption problem or because the media used features your browser did not support.'); break;
        case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
            errorToast(errors[404]); break;
        default:
            errorToast('An unknown media error occurred.');
    }
}

function switchLibrary(lib64) {
    fetch('http://achaplin.mynetgear.com:8080/charon-web/metadata/md_' + lib64 + '.json')
        .then(handleErrors)
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            var values = [];
            for (key in myJson) values.push(myJson[key]);
            try {
                userlist.clear();
            } catch (err) {}
            userlist = new List('main-songs-list', listTemplates['mainList'], values);
            sort();
            currentLib = lib64;

            search();
        });
}

function getSongUrl(sb64) {
    return baseUrl + currentLib + '/songs/' + sb64;
}