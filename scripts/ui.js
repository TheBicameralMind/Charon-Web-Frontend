// play song on double click
$(document).on('dblclick', 'li.mainList', function(e) {
    var res = tob64($(this));
    player.src = getSongUrl(res['b64']);

    $('#song-title').children()[0].innerText = res['title'];
    $('#song-artist').children()[0].innerText = res['artist'];
    if ($('.nowplaying')[0]) {
        $('.nowplaying')[0].classList.remove('nowplaying');
    }

    $(this).addClass('nowplaying');
    playPauseClick();
});

$(document).keydown(function(e) {
   if (e.target !== document.getElementById("search-bar")) { // can't do this in the search
       if (e.which == 32) { // space
           playPauseClick();
           if (e.stopPropagation) {
               e.stopPropagation(); 
               e.preventDefault();
           }
           return false;
       }
       if (e.which == 37) { // left arrow
           player.currentTime -= 5;
           return false;
       }
       if (e.which == 39) { // right arrow
           player.currentTime += 5;
           return false;
       }
   }
});

function highlight(e) {
    var type = e.classList[1]; 
    switch (type) {
        case 'mainList':
            var previous = $('#song-selected');
            if (previous.length != 0) {
                previous.removeAttr('id');
            }
            e.id = 'song-selected';
            break;
        case 'collectionList':
            var previous = $('#lib-selected');
            if (previous.length != 0){
                previous.removeAttr('id');
            }
            e.id = 'lib-selected';

            switchLibrary(btoa(e.children[0].innerText));
            break;
        case 'themesList': 
            if (!themes.hasOwnProperty(e.innerText)) return;

            var previous = $('#theme-selected'); 
            if (previous.length != 0) {
                previous.removeAttr('id');
            }
            e.id = 'theme-selected'; 
            
            setTheme(e.innerText); 
            updateThemePicker(e.innerText); 

            break; 
    }
}

function playPauseClick() {
    if (player.src === window.location.href) return;
    var button = document.getElementById("pp-button");
    if (player.paused) { // audio is paused
        button.firstElementChild.innerText = "pause";
        player.play();
    } else { // audio is playing
        if (player.currentTime < player.duration) {
            button.firstElementChild.innerText = "play_arrow";
            player.pause();
        }
    }
}

function toggleRepeat() {
    var button = document.getElementById('repeat');
    if (player.loop) {
        player.loop = !player.loop;
        button.firstElementChild.classList.add('md-inactive');
    } else {
        player.loop = !player.loop;
        button.firstElementChild.classList.remove('md-inactive');
    }
}

function toggleShuffle() {
    var button = document.getElementById('shuffle');
    if (shuffle) {
        shuffle = !shuffle;
        button.firstElementChild.classList.add('md-inactive');
    } else {
        shuffle = !shuffle;
        button.firstElementChild.classList.remove('md-inactive');
    }
}

function setRate(index, rate) {
    var children = document.getElementById("speed-container").children; // list all speeds
    for (var i = 0; i < children.length; i++) {
        children[i].style.fontWeight = "normal"; // unbold them all
    }
    children[index].style.fontWeight = "bolder"; // bold the clicked one
    prate = rate;
    player.playbackRate = prate;
}

function changeSearch(id) {
    var toBold = document.getElementById(id);
    var unBold = document.getElementById(id === "title-select" ? "artist-select" : "title-select");
    toBold.style.fontWeight = "bolder";
    unBold.style.fontWeight = "normal";
    toSearch = id === "title-select" ? ["title"] : ["artist"];
    search();
}

function search() {
    var value = $('#search-bar').val();
    userlist.search(value, toSearch);
}

function sort() {
    userlist.sort(sortby, order);
}

function openTab(evt, id) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tab");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("button-tab");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].children[0].classList.add("md-inactive");
    }

    document.getElementById(id).style.display = "block";
    evt.currentTarget.children[0].classList.remove("md-inactive");
}

function changeColor(varName, newColor) {
    currentEditedTheme[varName] = `#${newColor}`;
    less.modifyVars(currentEditedTheme); 
}

function setTheme(themeName) {
    currentThemeName = themeName; 
    less.modifyVars(themes[currentThemeName]); 
    currentEditedTheme = {...themes[currentThemeName]}
}

function updateThemePicker(themeName) {
    var colorPickers = document.getElementsByClassName('jscolor'); 
    var colors = Object.values(themes[themeName]); 
    for (var i = 0; i < colorPickers.length; i++) {
        colorPickers[i].jscolor.fromString(colors[i]);
    }
}

function sortSwitch(id) {
    var tsort = $('#tsort');
    var asort = $('#asort');

    if (id === 'tsort') { // clicked on title sort
        if (tsort.children()[0].classList.contains('invisible')) { // previously sorting by artist
            tsort.children()[0].classList.remove('invisible');
            asort.children()[0].classList.add('invisible');

            tsort.children()[0].classList.add('up');
            tsort.children()[0].classList.remove('down');

            sortby = 'title';
            order.order = 'asc';
            sort();
        } else { // currently sorting by title
            if (tsort.children()[0].classList.contains('up')) { // ascending order
                tsort.children()[0].classList.remove('up');
                tsort.children()[0].classList.add('down');

                sortby = 'title';
                order.order = 'desc';
                sort();
            } else { // descending order
                tsort.children()[0].classList.add('up');
                tsort.children()[0].classList.remove('down');

                sortby = 'title';
                order.order = 'asc';
                sort();
            }
        }
    } else {
        if (asort.children()[0].classList.contains('invisible')) { // previously sorting by title
            asort.children()[0].classList.remove('invisible');
            tsort.children()[0].classList.add('invisible');

            asort.children()[0].classList.add('up');
            asort.children()[0].classList.remove('down');

            sortby = 'artist';
            order.order = 'asc';
            sort();
        } else { // currently sorting by artist
            if (asort.children()[0].classList.contains('up')) { // ascending order
                asort.children()[0].classList.remove('up');
                asort.children()[0].classList.add('down');

                sortby = 'artist';
                order.order = 'desc';
                sort();
            } else { // descending order
                asort.children()[0].classList.add('up');
                asort.children()[0].classList.remove('down');

                sortby = 'artist';
                order.order = 'asc';
                sort();
            }
        }
    }
}

function launchToast() {
    var x = document.getElementById("toast");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}
