function skipBack() {
    if (player.currentTime > 10 || shuffle) {
        player.currentTime = 0; // restart song in the middle
    } else {
        previousOrderedSong();
    }
}

function skipForward() {
    if (shuffle) {
        nextRandomSong();
    }
    else {
        nextOrderedSong();
    }
}

function nextOrderedSong() {
    var next = $('.nowplaying').next();
    if (!next) return;
    var res = tob64(next);

    $('#song-title').children()[0].innerText = res['title'];
    $('#song-artist').children()[0].innerText = res['artist'];

    $('.nowplaying').removeClass('nowplaying');
    next.addClass('nowplaying');

    player.src = getSongUrl(res['b64']);
    playPauseClick();
    highlight(next[0]);
}

function previousOrderedSong() {
    var next = $('.nowplaying').prev();
    var res = tob64(next);

    $('#song-title').children()[0].innerText = res['title'];
    $('#song-artist').children()[0].innerText = res['artist'];

    $('.nowplaying').removeClass('nowplaying');
    next.addClass('nowplaying');

    player.src = getSongUrl(res['b64']);
    playPauseClick();
    highlight(next[0]);
}

function nextRandomSong() {
    var list = $('#main-songs-list').children().children();
    var nIndex = getRandomInt(0, list.length);
    var nLitem = list.get(nIndex);
    highlight(nLitem);
    var res = tob64($('#song-selected'));

    $('#song-title').children()[0].innerText = res['title'];
    $('#song-artist').children()[0].innerText = res['artist'];

    player.src = getSongUrl(res['b64']);
    playPauseClick();
}

function muteSwitch() {
    var button = document.getElementById('volume-button');
    if (button.firstElementChild.innerText === "volume_up") { // not muted
        button.firstElementChild.innerText = 'volume_off';
        player.volume = 0;
        vbar.slider('option', 'value', 0);
    } else { // muted
        button.firstElementChild.innerText = 'volume_up';
        player.volume = prevVolume;
        vbar.slider('option', 'value', prevVolume);
    }
}
