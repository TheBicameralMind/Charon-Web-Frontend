function onLoad() {
    $('#progress-bar').slider({
        range: 'min',
        max: 0,
        step: 1
    });

    $('#volume-slider').slider({
        range: 'min',
        max: 1,
        step: .01
    });

    $('#library-select').css("display", "block");

    player = document.getElementById('player');
    pbar = $('#progress-bar');
    vbar = $('#volume-slider');

    player.addEventListener('durationchange', function(e) {
        $('#total-time').text(formatSeconds(player.duration));
        pbar.slider("option", "max", player.duration);
        player.playbackRate = prate;
    });

    player.addEventListener('timeupdate', function(e) {
        $('#current-time').text(formatSeconds(player.currentTime));
        pbar.slider("option", "value", player.currentTime);
        if (player.currentTime >= player.duration) {
            document.getElementById("pp-button").firstElementChild.innerText = "play_arrow";
        }
    });

    player.addEventListener('ended', function(e) {
        if (shuffle) {
            nextRandomSong();
        } else {
            nextOrderedSong();
        }
    });

    player.addEventListener('error', handleMediaError);

    pbar.on('slide', function(e, ui) {
        if (e.originalEvent) { // mouse slides only lmao
           $('#current-time').text(formatSeconds(ui.value));
           player.currentTime = ui.value;
       }
    });

    vbar.on('slide', function(e, ui) {
        player.volume = ui.value;
        if (e.originalEvent) {
            prevVolume = ui.value;
        }
    });

    vbar.slider('option', 'value', 1);

    fetch('http://achaplin.mynetgear.com:8080/charon-web/metadata/md_collections.json')
        .then(handleErrors)
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            var values = [];
            for (var key in myJson) values.push({'title' : myJson[key]});
            
            var libList = new List('library-select', listTemplates['collectionList'], values);
            libList.sort('title', {order: 'asc'})
            highlight($('#library-select').children()[1].children[0], 2);
        });
    
    fetch('http://achaplin.mynetgear.com:8080/charon-web/styles/themes.json')
        .then(handleErrors)
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            themes = myJson; 
            currentThemeName = "Default";
            currentEditedTheme = {...themes["Default"]};
            
            var values = []; 
            for (var key in myJson) values.push({'title' : key});

            var themeList = new List('theme-select', listTemplates['themesList'], values);
            highlight($('#theme-select').children()[1].children[0], 2);
        })
    
    // theme = [{'title': 'Default'}]
    
}
