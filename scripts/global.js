var userlist;
var player;
var pbar;
var currentLib;
var themes; 
var currentThemeName;
var currentEditedTheme; 

var toSearch = ['title'];
var sortby = 'artist';
var order = {order: 'asc'};

var shuffle = false;
var repeat = false;
var prevVolume = 1;
var prate = 1;

var baseUrl = 'http://achaplin.mynetgear.com:8080/charon-web/collections/';

var errors = {
    404 : '404: Resource not found.'
};

var listTemplates = {
    'mainList' : {
        valueNames : ['title', 'artist', 'runtime'],
        item : "<li class='sl-item mainList' onclick='highlight(this)'><div class='title'></div><div class='artist'></div><div class='runtime'></div></li>"
    }, 

    'collectionList' : {
        valueNames : ['title'],
        item : "<li class='sl-item collectionList' onclick='highlight(this)'><div class='title'></div></li>"
    }, 

    'themesList' : {
        valueNames : ['title'], 
        item : "<li class='sl-item themesList' onclick='highlight(this)'><div class='title'></div></li>"
    }
}; 