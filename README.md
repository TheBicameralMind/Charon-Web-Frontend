# Charon-Web-Frontend
Charon Web is my attempt at creating a web player for me to use with my music. 
Originally, it started life as a JavaFX desktop application, but various
complications with media delivery and frontend design led me to switch to this 
web-based version. 

Until February 2020, it was hosted from a laptop in my guest bedroom, the
configuration for which can be found 
[here](https://gitlab.com/TheBicameralMind/Charon-Web-Server). Unfortunately, in
February 2020, that laptop suffered an unfortunate hard drive failure of the 
physical nature, and so Charon Web is no longer hosted anywhere.

With the exception of a few JS libraries ([List.js](https://listjs.com/), 
[jscolor](https://jscolor.com/), 
[Base64](https://gitlab.com/TheBicameralMind/Charon-Web-Frontend/-/blob/master/scripts/base64.js)),
everything else was written by me.